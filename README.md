# node-security

This library provides router and authentication middleware to register , 
authenticate and manage the user for a typical website.

## Installation

```
npm i @jebaraj.shenll/node-security
```


## Usage

```
// Set public RSA key 
process.env.JWT_PUBLIC_KEY
var security = require('@jebaraj.shenll/node-security');
app.use('/users', security.userRoute);
```

## Endpoints
### Login
```
POST /users/login
Content-Type: application/json

{"username":"username","password":"password"}
```

### List users
```
GET /users
```

### Register
```
POST /users
Content-Type: application/json

{"username":"username","password":"password"}
```