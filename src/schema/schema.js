const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const User = new Schema({
    username: {type: 'String'},
    password: {type: 'String'},
    email: {type: 'String'},
    emailVerified: {type: 'Boolean'},
    attributes: {type: 'String'}
});

const Role = new Schema({
    name: {type: 'String'},
    users: {type: Schema.Types.ObjectId, ref: 'User'},
    roles: {type: Schema.Types.ObjectId, ref: 'Role'},
});

const Session = new Schema({
    restricted: {type: 'Boolean'},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    installationId: {type: 'String'},
    sessionToken: {type: 'String'},
    expiresAt: {type: 'Date'},
    createdWith: {type: 'Object'},
});

module.exports = {
    User,
    Role,
    Session
}