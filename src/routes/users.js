const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Schema = require('../schema/schema');
const User = mongoose.model("User", Schema.User);
const passwordCrypto = require('../tools/password');
const jwt = require("../filter/JWTFilter");

class UsersRouter {
    constructor() {
        if (!process.env.JWT_PUBLIC_KEY) {
            throw new Error(
                "JWT public key missing!"
            )
        }
        if (!process.env.JWT_EXPIRE_TIME) {
            process.env.JWT_EXPIRE_TIME = '1800s'
        }
    }

    handleLogIn(req) {
        return this._authenticateUserFromRequest(req);
    }

    handleCreate(req) {
        return User.findOne({username: req.body.username}).then((user) => {
            if (user) {
                throw Error("User already available!")
            }
            return passwordCrypto.hash(req.body.password).then(hashedPassword => {
                const user = new User({
                    username: req.body.username,
                    password: hashedPassword,
                    email: req.body.email,
                    emailVerified: false,
                    attributes: JSON.stringify({})
                });
                return user.save();
            });
        });
    }

    handleGet(req) {
        return User.find({}).then((results) => {
            return results;
        });
    }

    _authenticateUserFromRequest(req) {
        return new Promise((resolve, reject) => {
            try {
                // Use query parameters instead if provided in url
                let payload = req.body;
                if (
                    (!payload.username && req.query.username) ||
                    (!payload.email && req.query.email)
                ) {
                    payload = req.query;
                }
                const {username, email, password} = payload;
                if (!username && !email) {
                    throw new Error(
                        'username/email is required.'
                    );
                }
                if (!password) {
                    throw new Error(
                        'password is required.'
                    );
                }
                if (
                    typeof password !== 'string' ||
                    (email && typeof email !== 'string') ||
                    (username && typeof username !== 'string')
                ) {
                    throw new Error(
                        'Invalid username/password.'
                    );
                }

                User.findOne({username: username}).then((user) => {
                    if (!user) {
                        throw new Error(
                            'Invalid username/password.'
                        );
                    } else {
                        if (passwordCrypto.compare(user.password, password)) {
                            resolve(user)
                        } else {
                            throw new Error(
                                'Invalid username/password.'
                            );
                        }
                    }
                });
            } catch (e) {
                reject(e)
            }
        });
    }
}

function mountRoutes(route) {
    // GET all users
    router.get('/', function (req, res) {
        route.handleGet(req).then((result) => {
            let objs = [];
            result.forEach((r) => {
                let obj = r.toObject();
                delete obj.password;
                objs.push(obj);
            });
            res.send(objs);
        }).catch((error) => {
            res.send(error.message);
            console.log(error)
        });
    });

    // Sign Up
    router.post('/', function (req, res, next) {
        route.handleCreate(req).then((result) => {
            let obj = result.toObject();
            delete obj.password;
            const r = {
                accessToken: jwt.generateAccessToken(obj)
            };
            res.send(r);
        }).catch((error) => {
            res.send(error.message);
            console.log(error)
        });
    });

    // Login
    router.post('/login', function (req, res) {
        route.handleLogIn(req).then((result) => {
            let obj = result.toObject();
            delete obj.password;
            const r = {
                accessToken: jwt.generateAccessToken(obj)
            };
            res.send(r);
        }).catch((error) => {
            res.send(error.message);
            console.log(error)
        });
    });
}

const userRoute = new UsersRouter();
mountRoutes(userRoute);

module.exports = router;
