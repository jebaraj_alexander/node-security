const jwt = require("jsonwebtoken");

function authenticate(req, res, next) {
    // Gather the jwt access token from the request header
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    // if there isn't any token
    if (token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.JWT_PUBLIC_KEY, (err, user) => {
        console.log(err);
        if (err) return res.sendStatus(403);
        req.user = user;
        next();
    })
}

function generateAccessToken(data) {
    // expires after half and hour (1800 seconds = 30 minutes)
    return jwt.sign(data, process.env.JWT_PUBLIC_KEY, {expiresIn: process.env.JWT_EXPIRE_TIME});
}

module.exports = {authenticate, generateAccessToken};